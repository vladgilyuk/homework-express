import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpExeption from '../application/exepetions/http-exeption';
import { ICourse } from './types/course.interface';
import { AppDataSource } from '../configs/database/data-source';
import { Course } from './entities/courses.entity';
import { UpdateResult, DeleteResult } from 'typeorm';
import { Lector } from '../lectors/entities/lectors.entity';

const coursesRepository = AppDataSource.getRepository(Course);
const lectorsRepository = AppDataSource.getRepository(Lector);

export const getAllCourses = async (): Promise<Course[]> => {
  const courses = await coursesRepository
    .createQueryBuilder('course')
    .select([
      'course.id as id',
      'course.createdAt as "createdAt"',
      'course.updatedAt as "updatedAt"',
      'course.name as name',
      'course.description as description',
      'course.hours as hours',
      'course.lastLectorId as lastLectorId',
    ])
    .getRawMany();
  if (courses.length === 0) {
    throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Courses not found');
  }

  return courses;
};

export const getCourseById = async (id: number): Promise<Course> => {
  const course = await coursesRepository.findOne({
    relations: {
      lectors: true,
    },
    where: { id },
  });

  if (!course) {
    throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Course not found');
  }

  return course;
};

export const createCourse = async (
  createCourseSchema: Omit<ICourse, 'id'>,
): Promise<Course> => {
  const course = await coursesRepository.findOne({
    where: {
      name: createCourseSchema.name,
    },
    relations: ['lectors'],
  });

  if (course) {
    throw new HttpExeption(
      HttpStatuses.BAD_REQUEST,
      'Course with this name already exists',
    );
  }

  return coursesRepository.save(createCourseSchema);
};

export const updatedCourseById = async (
  id: number,
  updateCourseSchema: Partial<ICourse>,
): Promise<UpdateResult> => {
  const result = await coursesRepository.update(id, updateCourseSchema);

  if (!result.affected) {
    throw new HttpExeption(
      HttpStatuses.NOT_FOUND,
      'Course with this id not found',
    );
  }

  return result;
};

export const AddLector = async (
  courseId: number,
  courseUpdateLectorIdSchema: Partial<ICourse>,
): Promise<UpdateResult> => {
  const { lastLectorId } = courseUpdateLectorIdSchema;

  const course = await coursesRepository.findOne({
    where: {
      id: courseId,
    },
    relations: ['lectors'],
  });

  if (!course) {
    throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Course not found');
  }

  if (lastLectorId) {
    const lector = await lectorsRepository.findOne({
      where: {
        id: lastLectorId,
      },
      relations: ['courses'],
    });

    if (!lector) {
      throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Lector not found');
    }

    course.lectors.push(lector);
  }

  await coursesRepository.save(course);

  const result = await coursesRepository.update(
    courseId,
    courseUpdateLectorIdSchema,
  );

  if (!result.affected) {
    throw new HttpExeption(
      HttpStatuses.NOT_FOUND,
      'Course with this id not found',
    );
  }

  return result;
};

export const deleteCourseById = async (id: number): Promise<DeleteResult> => {
  const result = await coursesRepository.delete(id);

  if (!result.affected) {
    throw new HttpExeption(
      HttpStatuses.NOT_FOUND,
      'Course with this id not found',
    );
  }

  return result;
};
