import { Router } from 'express';
import * as coursesController from './courses.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import {
  courseCreateSchema,
  courseUpdateSchema,
  courseUpdateLectorIdSchema,
} from './course.schema';

const router = Router();

router.get('/', controllerWrapper(coursesController.getAllCourses));
router.get('/:id', controllerWrapper(coursesController.getCourseById));
router.post(
  '/',
  validator.body(courseCreateSchema),
  controllerWrapper(coursesController.createCourse),
);
router.patch(
  '/:id',
  validator.body(courseUpdateSchema),
  controllerWrapper(coursesController.updateCourseById),
);
router.patch(
  '/:id/lector',
  validator.body(courseUpdateLectorIdSchema),
  controllerWrapper(coursesController.addLector),
);
router.delete('/:id', controllerWrapper(coursesController.deleteCourseById));

export default router;
