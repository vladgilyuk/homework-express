import { NextFunction, Request, Response } from 'express';
import * as courseService from './courses.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IcourseUpdateRequest } from './types/course-update-request.interface copy';
import { IcourseCreateRequest } from './types/course-create-request.interface';
import { HttpStatuses } from '../application/enums/http-statuses.enum';

export const getAllCourses = async (request: Request, response: Response) => {
  const courses = await courseService.getAllCourses();
  response.json(courses);
};

export const getCourseById = async (
  request: Request<{ id: number }>,
  response: Response,
  next: NextFunction,
) => {
  const course = await courseService.getCourseById(request.params.id);
  response.json(course);
};

export const createCourse = async (
  request: ValidatedRequest<IcourseCreateRequest>,
  response: Response,
) => {
  const course = await courseService.createCourse(request.body);
  response.statusCode = HttpStatuses.CREATED;
  response.json(course);
};

export const updateCourseById = async (
  request: ValidatedRequest<IcourseUpdateRequest>,
  response: Response,
) => {
  const course = await courseService.updatedCourseById(
    request.params.id,
    request.body,
  );
  response.json(course);
};
export const addLector = async (
  request: ValidatedRequest<IcourseUpdateRequest>,
  response: Response,
) => {
  const course = await courseService.AddLector(request.params.id, request.body);
  response.json(course);
};

export const deleteCourseById = async (
  request: Request<{ id: number }>,
  response: Response,
) => {
  const course = await courseService.deleteCourseById(request.params.id);
  response.json(course);
};
