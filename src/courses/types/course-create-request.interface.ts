import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { ICourse } from './course.interface';

export interface IcourseCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<ICourse, 'id'>;
}
