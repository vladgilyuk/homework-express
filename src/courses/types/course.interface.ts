export interface ICourse {
  id: number;
  name: string;
  description: string;
  hours: number;
  lastLectorId: number | null;
}
