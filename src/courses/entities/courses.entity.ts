import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Lector } from '../../lectors/entities/lectors.entity';
import { Mark } from '../../marks/entities/mark.entity';

@Entity({ name: 'courses' })
export class Course extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  description: string;

  @Column({
    type: 'numeric',
    nullable: false,
  })
  hours: number;

  @Column({
    type: 'integer',
    nullable: true,
    name: 'lector_id',
  })
  lastLectorId: number | null;

  @OneToMany(() => Mark, (mark) => mark.course)
  marks: Mark[];

  @ManyToMany(() => Lector, (lector) => lector.courses, {
    nullable: true,
    eager: false,
  })
  @JoinTable({
    name: 'lector_course',
    joinColumn: {
      name: 'course_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'lector_id',
      referencedColumnName: 'id',
    },
  })
  lectors: Lector[];
}
