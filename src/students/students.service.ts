import ObjectID from 'bson-objectid';
import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpExeption from '../application/exepetions/http-exeption';
import { IStudent } from './types/student.interface';
//import path from 'path';
//import fs from 'fs/promises';
import { AppDataSource } from '../configs/database/data-source';
import { Student } from './entities/student.entity';
import { UpdateResult, DeleteResult } from 'typeorm';
import { Group } from '../groups/entities/group.entity';

const studentsRepository = AppDataSource.getRepository(Student);

export const getAllStudents = async (name?: string): Promise<Student[]> => {
  const queryBuilder = studentsRepository
    .createQueryBuilder('student')
    .select([
      'student.id as id',
      'student.createdAt as "createdAt"',
      'student.updatedAt as "updatedAt"',
      'student.name as name',
      'student.surname as surname',
      'student.email as email',
      'student.age as age',
      'student.image_path as "imagePath"',
      'student.groupId as "groupId"',
    ])
    .leftJoin('student.group', 'group')
    .addSelect('group.name as "groupName"');

  if (name) {
    queryBuilder.where('student.name = :name', { name });
  }

  const students = await queryBuilder.getRawMany();

  if (students.length === 0) {
    throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Students not found');
  }

  return students;
};

export const getStudentById = async (id: number): Promise<Student> => {
  const student = await studentsRepository
    .createQueryBuilder('student')
    .select([
      'student.id as id',
      'student.createdAt as "createdAt"',
      'student.updatedAt as "updatedAt"',
      'student.name as name',
      'student.surname as surname',
      'student.email as email',
      'student.age as age',
      'student.image_path as "imagePath"',
      'student.groupId as "groupId"',
    ])
    .leftJoin('student.group', 'group')
    .addSelect('group.name as "groupName"')
    .where('student.id = :id', { id })
    .getRawOne();

  if (!student) {
    throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  return student;
};

export const createStudent = async (
  createStudentSchema: Omit<IStudent, 'id'>,
): Promise<Student> => {
  const student = await studentsRepository.findOne({
    where: {
      email: createStudentSchema.email,
    },
  });

  if (student) {
    throw new HttpExeption(
      HttpStatuses.BAD_REQUEST,
      'Student with this email already exists',
    );
  }

  if (createStudentSchema.groupId) {
    const group = await AppDataSource.getRepository(Group).findOne({
      where: {
        id: createStudentSchema.groupId,
      },
    });

    if (!group) {
      throw new HttpExeption(
        HttpStatuses.BAD_REQUEST,
        'Group with this ID does not exist',
      );
    }
  }

  return studentsRepository.save(createStudentSchema);
};

export const updatedStudentById = async (
  id: number,
  updateStudentSchema: Partial<IStudent>,
): Promise<UpdateResult> => {
  const result = await studentsRepository.update(id, updateStudentSchema);

  if (!result.affected) {
    throw new HttpExeption(
      HttpStatuses.NOT_FOUND,
      'Student with this id not found',
    );
  }

  return result;
};

export const AddGroup = async (
  id: number,
  studentUpdateGroupIdSchema: Partial<IStudent>,
): Promise<UpdateResult> => {
  const { groupId } = studentUpdateGroupIdSchema;

  if (groupId) {
    const group = await AppDataSource.getRepository(Group).findOne({
      where: {
        id: groupId,
      },
    });

    if (!group) {
      throw new HttpExeption(
        HttpStatuses.BAD_REQUEST,
        'Group with this ID does not exist',
      );
    }
  }

  const result = await studentsRepository.update(
    id,
    studentUpdateGroupIdSchema,
  );

  if (!result.affected) {
    throw new HttpExeption(
      HttpStatuses.NOT_FOUND,
      'Student with this id not found',
    );
  }

  return result;
};

/*
export const addImage = async (id: number, filePath?: string) => {
  if (!filePath) {
    throw new HttpExeption(HttpStatuses.BAD_REQUEST, 'File is not provided');
  }
  try {
    const imageId = ObjectID().toHexString();
    const imageExtention = path.extname(filePath);
    const imageName = imageId + imageExtention;

    const studentsDirectoryName = 'students';
    const studentsDirectoryPath = path.join(
      __dirname,
      '../',
      'public',
      studentsDirectoryName,
    );
    const newImagePath = path.join(studentsDirectoryPath, imageName);
    const imagePath = `${studentsDirectoryName}/${imageName}`;

    await fs.rename(filePath, newImagePath);

    const updatedStudent = updatedStudentById(id, { imagePath });

    return updatedStudent;
  } catch (error) {
    await fs.unlink(filePath);
    throw error;
  }
};
*/
export const deleteStudentById = async (id: number): Promise<DeleteResult> => {
  const result = await studentsRepository.delete(id);

  if (!result.affected) {
    throw new HttpExeption(
      HttpStatuses.NOT_FOUND,
      'Student with this id not found',
    );
  }

  return result;
};
