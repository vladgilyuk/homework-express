import { Router, query } from 'express';
import * as studentsController from './students.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import {
  studentCreateSchema,
  studentUpdateSchema,
  studentUpdateGroupIdSchema,
} from './student.schema';

const router = Router();

router.get('/', controllerWrapper(studentsController.getAllStudents));
router.get('/:id', controllerWrapper(studentsController.getStudentById));
router.post(
  '/',
  validator.body(studentCreateSchema),
  controllerWrapper(studentsController.createStudent),
);
router.patch(
  '/:id',
  validator.body(studentUpdateSchema),
  controllerWrapper(studentsController.updateStudentById),
); /*
router.patch(
  '/:id/image',
  uploadMiddleware.single('file'),
  controllerWrapper(studentsController.addImage),
);*/
router.patch(
  '/:id/group',
  validator.body(studentUpdateGroupIdSchema),
  controllerWrapper(studentsController.addGroup),
);
router.delete('/:id', controllerWrapper(studentsController.deleteStudentById));

export default router;
