import { NextFunction, Request, Response } from 'express';
import * as studentsService from './students.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IstudentUpdateRequest } from './types/student-update-request.interface copy';
import { IstudentCreateRequest } from './types/student-create-request.interface';
import { HttpStatuses } from '../application/enums/http-statuses.enum';

export const getAllStudents = async (request: Request, response: Response) => {
  const name = request.query.name as string; // Explicitly cast the query parameter to string
  const students = await studentsService.getAllStudents(name);
  response.json(students);
};

export const getStudentById = async (
  request: Request<{ id: number }>,
  response: Response,
  next: NextFunction,
) => {
  const student = await studentsService.getStudentById(request.params.id);
  response.json(student);
};

export const createStudent = async (
  request: ValidatedRequest<IstudentCreateRequest>,
  response: Response,
) => {
  const student = await studentsService.createStudent(request.body);
  response.statusCode = HttpStatuses.CREATED;
  response.json(student);
};

export const updateStudentById = async (
  request: ValidatedRequest<IstudentUpdateRequest>,
  response: Response,
) => {
  const student = await studentsService.updatedStudentById(
    request.params.id,
    request.body,
  );
  response.json(student);
};
export const addGroup = async (
  request: ValidatedRequest<IstudentUpdateRequest>,
  response: Response,
) => {
  const student = await studentsService.AddGroup(
    request.params.id,
    request.body,
  );
  response.json(student);
}; /*
export const addImage = (
  request: Request<{ id: number; file: Express.Multer.File }>,
  response: Response,
) => {
  const { id } = request.params;
  const { path } = request.file ?? {};
  const student = studentsService.addImage(id, path);
  response.json(student);
};*/

export const deleteStudentById = async (
  request: Request<{ id: number }>,
  response: Response,
) => {
  const student = await studentsService.deleteStudentById(request.params.id);
  response.json(student);
};
