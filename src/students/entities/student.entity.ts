import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Group } from '../../groups/entities/group.entity';
import { Mark } from '../../marks/entities/mark.entity';

@Entity({ name: 'students' })
@Index('idx_name', ['name'])
export class Student extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  surname: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  email: string;

  @Column({
    type: 'numeric',
    nullable: false,
  })
  age: number;

  @Column({
    name: 'image_path',
    type: 'varchar',
    nullable: false,
  })
  imagePath: string;

  @OneToMany(() => Mark, (mark) => mark.student)
  marks: Mark[];

  @ManyToOne(() => Group, (group) => group.students, {
    nullable: false,
    eager: false,
  })
  @JoinColumn({ name: 'group_id' })
  group: Group;

  @Column({
    type: 'integer',
    nullable: true,
    name: 'group_id',
  })
  groupId: number | null;
}
