import Joi from 'joi';

export const idParamsSchema = Joi.object<{ id: number }>({
  id: Joi.number().required(),
});
