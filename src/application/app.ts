import express from 'express';
import cors from 'cors';
import logger from './middlewares/logger.middleware';
import studentsRouter from '../students/students.router';
import groupsRouter from '../groups/groups.router';
import coursesRouter from '../courses/courses.router';
import bodyParser from 'body-parser';
import exepetionsFilter from './middlewares/expetions.filter.ts';
import path from 'path';
import { AppDataSource } from '../configs/database/data-source';
import { error } from 'console';
import lectorsRouter from '../lectors/lectors.router';
import marksRouter from '../marks/marks.router';

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(logger);

AppDataSource.initialize()
  .then(() => {
    console.log('Typeorm connected to database');
  })
  .catch(() => {
    console.log('Error: ', error);
  });
const staticFilesPath = path.join(__dirname, '../', 'public');

app.use('/api/v1/public', express.static(staticFilesPath));
app.use('/api/v1/students', studentsRouter);
app.use('/api/v1/groups', groupsRouter);
app.use('/api/v1/courses', coursesRouter);
app.use('/api/v1/lectors', lectorsRouter);
app.use('/api/v1/marks', marksRouter);

app.use(exepetionsFilter);

export default app;
