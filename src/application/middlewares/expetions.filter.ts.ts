import { NextFunction, Request, Response } from 'express';
import HttpExeption from '../exepetions/http-exeption';
import { HttpStatuses } from '../enums/http-statuses.enum';

const exepetionsFilter = (
  error: HttpExeption,
  request: Request,
  response: Response,
  next: NextFunction,
) => {
  const status = error.status || HttpStatuses.INTERNAL_SERVER_ERROR;
  const message = error.message || 'Something went wrong';
  response.status(status).send({ status, message });
};

export default exepetionsFilter;
