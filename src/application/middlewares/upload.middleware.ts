import path from 'path';
import multer from 'multer';

const multerDestination = path.join(__dirname, '../../', 'temp');

const multerConfig = multer.diskStorage({
  destination: multerDestination,
  filename: (request, file, callback) => {
    callback(null, file.originalname);
  },
});

const uploadMiddleware = multer({ storage: multerConfig });

export default uploadMiddleware;
