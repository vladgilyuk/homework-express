import { NextFunction, Request, Response } from 'express';
import * as lectorsService from './lectors.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IlectorCreateRequest } from './types/lector-create-request.interface';
import { IlectorUpdateRequest } from './types/lector-update-request.interface copy';
import { HttpStatuses } from '../application/enums/http-statuses.enum';

export const getAllLectors = async (request: Request, response: Response) => {
  const lectors = await lectorsService.getAllLectors();
  response.json(lectors);
};

export const getLectorById = async (
  request: Request<{ id: number }>,
  response: Response,
  next: NextFunction,
) => {
  const lector = await lectorsService.getLectorById(request.params.id);
  response.json(lector);
};

export const getLectorCoursesById = async (
  request: Request<{ id: number }>,
  response: Response,
  next: NextFunction,
) => {
  const lector = await lectorsService.getLectorCoursesById(request.params.id);
  response.json(lector);
};

export const createLector = async (
  request: ValidatedRequest<IlectorCreateRequest>,
  response: Response,
) => {
  const lector = await lectorsService.createLector(request.body);
  response.statusCode = HttpStatuses.CREATED;
  response.json(lector);
};

export const updateLectorById = async (
  request: ValidatedRequest<IlectorUpdateRequest>,
  response: Response,
) => {
  const lector = await lectorsService.updatedLectorById(
    request.params.id,
    request.body,
  );
  response.json(lector);
};

export const deleteLectorById = async (
  request: Request<{ id: number }>,
  response: Response,
) => {
  const lector = await lectorsService.deleteLectorById(request.params.id);
  response.json(lector);
};
