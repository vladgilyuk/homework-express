import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { ILector } from './lector.interface';

export interface IlectorCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<ILector, 'id'>;
}
