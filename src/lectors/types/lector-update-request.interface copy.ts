import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { ILector } from './lector.interface';

export interface IlectorUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<ILector>;
}
