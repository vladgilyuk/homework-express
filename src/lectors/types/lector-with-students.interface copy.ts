import { ICourse } from '../../courses/types/course.interface';
export interface ILectorWithCourses {
  id: number;
  name: string;
  email: string;
  password: string;
  coursesInLector: ICourse[];
}
