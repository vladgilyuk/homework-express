import { DeleteResult, UpdateResult } from 'typeorm';
import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpExeption from '../application/exepetions/http-exeption';
import { AppDataSource } from '../configs/database/data-source';
import { Lector } from './entities/lectors.entity';
import { ILector } from './types/lector.interface';
import { Course } from '../courses/entities/courses.entity';

const lectorsRepository = AppDataSource.getRepository(Lector);
const coursesRepository = AppDataSource.getRepository(Course);

export const getAllLectors = async (): Promise<Lector[]> => {
  const lectors = await lectorsRepository
    .createQueryBuilder('lector')
    .select([
      'lector.id as id',
      'lector.createdAt as "createdAt"',
      'lector.updatedAt as "updatedAt"',
      'lector.name as name',
      'lector.email as email',
      'lector.password as password',
    ])
    .getRawMany();

  if (lectors.length === 0) {
    throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Lectors not found');
  }

  return lectors;
};

export const getLectorById = async (id: number): Promise<Lector> => {
  const lector = await lectorsRepository.findOne({
    relations: {
      courses: true,
    },
    where: { id },
  });

  if (!lector) {
    throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Lector not found');
  }

  return lector;
};

export const getLectorCoursesById = async (id: number): Promise<Course[]> => {
  const lector = await lectorsRepository.findOne({
    relations: {
      courses: true,
    },
    where: { id },
  });

  if (!lector) {
    throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Lector not found');
  }

  return lector.courses;
};

export const createLector = async (
  createLectorSchema: Omit<ILector, 'id'>,
): Promise<Lector> => {
  const lector = await lectorsRepository.findOne({
    where: {
      name: createLectorSchema.name,
    },
    relations: ['courses'],
  });

  if (lector) {
    throw new HttpExeption(
      HttpStatuses.BAD_REQUEST,
      'Lector with this name already exists',
    );
  }

  return lectorsRepository.save(createLectorSchema);
};

export const updatedLectorById = async (
  id: number,
  upadateLectorSchema: Partial<ILector>,
): Promise<UpdateResult> => {
  const lector = await lectorsRepository.findOne({
    where: {
      name: upadateLectorSchema.name,
    },
  });

  if (lector) {
    throw new HttpExeption(
      HttpStatuses.BAD_REQUEST,
      'Lector with this name already exists',
    );
  }

  const result = await lectorsRepository.update(id, upadateLectorSchema);

  if (!result.affected) {
    throw new HttpExeption(
      HttpStatuses.NOT_FOUND,
      'Lector with this id not found',
    );
  }

  return result;
};

export const deleteLectorById = async (id: number): Promise<DeleteResult> => {
  const lector = await lectorsRepository.findOne({
    relations: {
      courses: true,
    },
    where: { id },
  });

  if (!lector) {
    throw new HttpExeption(
      HttpStatuses.NOT_FOUND,
      'Lector with this id not found',
    );
  }

  if (lector.courses) {
    for (const course of lector.courses) {
      if (course.lectors) {
        course.lectors = course.lectors.filter((lector) => lector.id !== id);
        await coursesRepository.save(course);
      }
    }
  }

  return lectorsRepository.delete(id);
};
