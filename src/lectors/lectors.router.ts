import { Router } from 'express';
import * as lectorsController from './lectors.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { lectorCreateSchema, lectorUpdateSchema } from './lector.schema';

const router = Router();

router.get('/', controllerWrapper(lectorsController.getAllLectors));
router.get('/:id', controllerWrapper(lectorsController.getLectorById));
router.get(
  '/:id/courses',
  controllerWrapper(lectorsController.getLectorCoursesById),
);
router.post(
  '/',
  validator.body(lectorCreateSchema),
  controllerWrapper(lectorsController.createLector),
);
router.patch(
  '/:id',
  validator.body(lectorUpdateSchema),
  controllerWrapper(lectorsController.updateLectorById),
);
router.delete('/:id', controllerWrapper(lectorsController.deleteLectorById));

export default router;
