import { DataSource } from 'typeorm';
import { databaseConfiguration } from './datavase-config';

export const AppDataSource = new DataSource(databaseConfiguration());
