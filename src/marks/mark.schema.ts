import Joi from 'joi';
import { IMark } from './types/mark.interface';

export const markCreateSchema = Joi.object<Omit<IMark, 'id'>>({
  studentId: Joi.number().required(),
  lectorId: Joi.number().required(),
  courseId: Joi.number().required(),
  mark: Joi.number().required(),
});

export const markUpdateSchema = Joi.object<Partial<IMark>>({
  studentId: Joi.number().optional(),
  lectorId: Joi.number().optional(),
  courseId: Joi.number().optional(),
  mark: Joi.number().optional(),
});
