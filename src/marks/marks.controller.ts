import { NextFunction, Request, Response } from 'express';
import * as marksService from './marks.service';
import { ValidatedRequest } from 'express-joi-validation';
import { ImarkUpdateRequest } from './types/mark-update-request.interface copy';
import { ImarkCreateRequest } from './types/mark-create-request.interface';
import { HttpStatuses } from '../application/enums/http-statuses.enum';

export const getAllMarks = async (request: Request, response: Response) => {
  const marks = await marksService.getAllMarks();
  response.json(marks);
};

export const getMarkById = async (
  request: Request<{ id: number }>,
  response: Response,
  next: NextFunction,
) => {
  const mark = await marksService.getMarkById(request.params.id);
  response.json(mark);
};

export const getMarksByStudentId = async (
  request: Request<{ id: number }>,
  response: Response,
  next: NextFunction,
) => {
  const mark = await marksService.getMarksByStudentId(request.params.id);
  response.json(mark);
};

export const getMarksByCourseId = async (
  request: Request<{ id: number }>,
  response: Response,
  next: NextFunction,
) => {
  const mark = await marksService.getMarksByCourseId(request.params.id);
  response.json(mark);
};

export const createMark = async (
  request: ValidatedRequest<ImarkCreateRequest>,
  response: Response,
) => {
  const mark = await marksService.createMark(request.body);
  response.statusCode = HttpStatuses.CREATED;
  response.json(mark);
};

export const updateMarkById = async (
  request: ValidatedRequest<ImarkUpdateRequest>,
  response: Response,
) => {
  const mark = await marksService.updateMarkById(
    request.params.id,
    request.body,
  );
  response.json(mark);
};

export const deleteMarkById = async (
  request: Request<{ id: number }>,
  response: Response,
) => {
  const mark = await marksService.deleteMarkById(request.params.id);
  response.json(mark);
};
