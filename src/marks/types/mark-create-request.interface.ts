import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IMark } from './mark.interface';

export interface ImarkCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<IMark, 'id'>;
}
