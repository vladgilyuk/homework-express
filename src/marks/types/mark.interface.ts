export interface IMark {
  id: number;
  mark: number;
  studentId: number;
  lectorId: number;
  courseId: number;
}
