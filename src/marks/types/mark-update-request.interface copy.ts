import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IMark } from './mark.interface';

export interface ImarkUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<IMark>;
}
