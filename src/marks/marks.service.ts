import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpExeption from '../application/exepetions/http-exeption';
import { IMark } from './types/mark.interface';
import { AppDataSource } from '../configs/database/data-source';
import { Mark } from './entities/mark.entity';
import { UpdateResult, DeleteResult } from 'typeorm';
import { Student } from '../students/entities/student.entity';
import { Lector } from '../lectors/entities/lectors.entity';
import { Course } from '../courses/entities/courses.entity';

const marksRepository = AppDataSource.getRepository(Mark);

export const getAllMarks = async (): Promise<Mark[]> => {
  const marks = await marksRepository
    .createQueryBuilder('mark')
    .select([
      'mark.id as id',
      'mark.createdAt as "createdAt"',
      'mark.updatedAt as "updatedAt"',
      'mark.mark as mark',
      'mark.student_id as "studentId"',
      'mark.lector_Id as "lectorId"',
      'mark.course_Id as "courseId"',
    ])
    .getRawMany();
  if (marks.length === 0) {
    throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Marks not found');
  }

  return marks;
};

export const getMarkById = async (id: number): Promise<Mark> => {
  const mark = await marksRepository
    .createQueryBuilder('mark')
    .select([
      'mark.id as id',
      'mark.createdAt as "createdAt"',
      'mark.updatedAt as "updatedAt"',
      'mark.mark as mark',
      'mark.student_id as "studentId"',
      'mark.lector_Id as "lectorId"',
      'mark.course_Id as "courseId"',
    ])
    .where('mark.id = :id', { id })
    .getRawOne();

  if (!mark) {
    throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Mark not found');
  }

  return mark;
};

export const getMarksByStudentId = async (
  studentId: number,
): Promise<Mark[]> => {
  const marks = await marksRepository
    .createQueryBuilder('mark')
    .select(['mark.mark as mark', 'course.name as "courseName"'])
    .innerJoin('mark.course', 'course')
    .where('mark.student_id = :studentId', { studentId })
    .getRawMany();

  if (marks.length === 0) {
    throw new HttpExeption(
      HttpStatuses.NOT_FOUND,
      'Marks not found for the given student',
    );
  }

  return marks;
};

export const getMarksByCourseId = async (courseId: number): Promise<Mark[]> => {
  const marks = await marksRepository
    .createQueryBuilder('mark')
    .leftJoinAndSelect('mark.course', 'course')
    .leftJoinAndSelect('mark.lector', 'lector')
    .leftJoinAndSelect('mark.student', 'student')
    .where('course.id = :courseId', { courseId })
    .select([
      'course.name as "courseName"',
      'lector.name as "lectorName"',
      'student.name as "studentName"',
      'mark.mark as mark',
    ])
    .getRawMany();

  if (marks.length === 0) {
    throw new HttpExeption(
      HttpStatuses.NOT_FOUND,
      'Marks not found for the given course',
    );
  }
  return marks;
};

export const createMark = async (
  createMarkData: Omit<IMark, 'id'>,
): Promise<Mark> => {
  const student = await AppDataSource.getRepository(Student).findOne({
    where: {
      id: createMarkData.studentId,
    },
  });
  if (!student) {
    throw new HttpExeption(
      HttpStatuses.BAD_REQUEST,
      'Student with this ID does not exist',
    );
  }

  const lector = await AppDataSource.getRepository(Lector).findOne({
    where: {
      id: createMarkData.lectorId,
    },
  });
  if (!lector) {
    throw new HttpExeption(
      HttpStatuses.BAD_REQUEST,
      'Lector with this ID does not exist',
    );
  }

  const course = await AppDataSource.getRepository(Course).findOne({
    where: {
      id: createMarkData.courseId,
    },
  });
  if (!course) {
    throw new HttpExeption(
      HttpStatuses.BAD_REQUEST,
      'Course with this ID does not exist',
    );
  }

  const mark = new Mark();
  mark.mark = createMarkData.mark;
  mark.student = student;
  mark.lector = lector;
  mark.course = course;

  return marksRepository.save(mark);
};

export const updateMarkById = async (
  id: number,
  updateMarkData: Partial<IMark>,
): Promise<UpdateResult> => {
  const result = await marksRepository.update(id, updateMarkData);

  if (result.affected === 0) {
    throw new HttpExeption(
      HttpStatuses.NOT_FOUND,
      'Mark with this ID not found',
    );
  }

  return result;
};

export const deleteMarkById = async (id: number): Promise<DeleteResult> => {
  const result = await marksRepository.delete(id);

  if (result.affected === 0) {
    throw new HttpExeption(
      HttpStatuses.NOT_FOUND,
      'Mark with this ID not found',
    );
  }

  return result;
};
