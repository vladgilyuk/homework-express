import { Router } from 'express';
import * as marksController from './marks.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { markCreateSchema, markUpdateSchema } from './mark.schema';

const router = Router();

router.get('/', controllerWrapper(marksController.getAllMarks));
router.get('/:id', controllerWrapper(marksController.getMarkById));
router.get(
  '/student/:id',
  controllerWrapper(marksController.getMarksByStudentId),
);
router.get(
  '/course/:id',
  controllerWrapper(marksController.getMarksByCourseId),
);
router.post(
  '/',
  validator.body(markCreateSchema),
  controllerWrapper(marksController.createMark),
);
router.patch(
  '/:id',
  validator.body(markUpdateSchema),
  controllerWrapper(marksController.updateMarkById),
);
router.delete('/:id', controllerWrapper(marksController.deleteMarkById));

export default router;
