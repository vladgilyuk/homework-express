import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Course } from '../../courses/entities/courses.entity';
import { Student } from '../../students/entities/student.entity';
import { Lector } from '../../lectors/entities/lectors.entity';

@Entity({ name: 'marks' })
export class Mark extends CoreEntity {
  @Column({ type: 'integer', nullable: false, name: 'mark' })
  mark: number;

  @ManyToOne(() => Course, (course) => course.marks, {
    nullable: false,
    eager: false,
  })
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @ManyToOne(() => Student, (student) => student.marks, {
    nullable: false,
    eager: false,
  })
  @JoinColumn({ name: 'student_id' })
  student: Student;

  @ManyToOne(() => Lector, (lector) => lector.marks, {
    nullable: false,
    eager: false,
  })
  @JoinColumn({ name: 'lector_id' })
  lector: Lector;
}
