import { Router } from 'express';
import * as groupsController from './groups.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { groupCreateSchema, groupUpdateSchema } from './group.schema';

const router = Router();

router.get('/', controllerWrapper(groupsController.getAllGroups));
router.get('/:id', controllerWrapper(groupsController.getGroupById));
router.post(
  '/',
  validator.body(groupCreateSchema),
  controllerWrapper(groupsController.createGroup),
);
router.patch(
  '/:id',
  validator.body(groupUpdateSchema),
  controllerWrapper(groupsController.updateGroupById),
);
router.delete('/:id', controllerWrapper(groupsController.deleteGroupById));

export default router;
