import { IStudent } from '../../students/types/student.interface';

export interface IGroupWithStudents {
  id: number;
  name: string;
  studentsInGroup: IStudent[];
}
