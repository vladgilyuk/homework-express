import { DeleteResult, UpdateResult } from 'typeorm';
import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpExeption from '../application/exepetions/http-exeption';
import { AppDataSource } from '../configs/database/data-source';
import { Group } from './entities/group.entity';
import { IGroup } from './types/group.interface';
import { Student } from '../students/entities/student.entity';

const groupsRepository = AppDataSource.getRepository(Group);
const studentsRepository = AppDataSource.getRepository(Student);

export const getAllGroups = async (): Promise<Group[]> => {
  const groups = await groupsRepository
    .createQueryBuilder('group')
    .leftJoinAndSelect('group.students', 'student')
    .getMany();

  if (groups.length === 0) {
    throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Groups not found');
  }

  return groups;
};

export const getGroupById = async (id: number): Promise<Group> => {
  const group = await groupsRepository
    .createQueryBuilder('group')
    .leftJoinAndSelect('group.students', 'student')
    .where('group.id = :id', { id })
    .getOne();

  if (!group) {
    throw new HttpExeption(HttpStatuses.NOT_FOUND, 'Group not found');
  }

  return group;
};

export const createGroup = async (
  createGroupSchema: Omit<IGroup, 'id'>,
): Promise<Group> => {
  const group = await groupsRepository.findOne({
    where: {
      name: createGroupSchema.name,
    },
  });

  if (group) {
    throw new HttpExeption(
      HttpStatuses.BAD_REQUEST,
      'Group with this name already exists',
    );
  }

  return groupsRepository.save(createGroupSchema);
};

export const updatedGroupById = async (
  id: number,
  upadateGroupSchema: Partial<IGroup>,
): Promise<UpdateResult> => {
  const group = await groupsRepository.findOne({
    where: {
      name: upadateGroupSchema.name,
    },
  });

  if (group) {
    throw new HttpExeption(
      HttpStatuses.BAD_REQUEST,
      'Group with this name already exists',
    );
  }

  const result = await groupsRepository.update(id, upadateGroupSchema);

  if (!result.affected) {
    throw new HttpExeption(
      HttpStatuses.NOT_FOUND,
      'Group with this id not found',
    );
  }

  return result;
};

export const deleteGroupById = async (id: number): Promise<DeleteResult> => {
  const group = await groupsRepository
    .createQueryBuilder('group')
    .leftJoinAndSelect('group.students', 'student')
    .where('group.id = :id', { id })
    .getOne();

  if (!group) {
    throw new HttpExeption(
      HttpStatuses.NOT_FOUND,
      'Group with this id not found',
    );
  }

  for (const student of group.students) {
    student.groupId = null;
    await studentsRepository.save(student);
  }

  return groupsRepository.delete(id);
};
