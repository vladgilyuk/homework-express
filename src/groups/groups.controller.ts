import { NextFunction, Request, Response } from 'express';
import * as groupsService from './groups.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IgroupCreateRequest } from './types/group-create-request.interface';
import { IgroupUpdateRequest } from './types/group-update-request.interface copy';
import { HttpStatuses } from '../application/enums/http-statuses.enum';

export const getAllGroups = async (request: Request, response: Response) => {
  const groups = await groupsService.getAllGroups();
  response.json(groups);
};

export const getGroupById = async (
  request: Request<{ id: number }>,
  response: Response,
  next: NextFunction,
) => {
  const group = await groupsService.getGroupById(request.params.id);
  response.json(group);
};

export const createGroup = async (
  request: ValidatedRequest<IgroupCreateRequest>,
  response: Response,
) => {
  const group = await groupsService.createGroup(request.body);
  response.statusCode = HttpStatuses.CREATED;
  response.json(group);
};

export const updateGroupById = async (
  request: ValidatedRequest<IgroupUpdateRequest>,
  response: Response,
) => {
  const group = await groupsService.updatedGroupById(
    request.params.id,
    request.body,
  );
  response.json(group);
};

export const deleteGroupById = async (
  request: Request<{ id: number }>,
  response: Response,
) => {
  const group = await groupsService.deleteGroupById(request.params.id);
  response.json(group);
};
